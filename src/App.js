import "./App.css";
import React from "react";
import Header from "./component/Header";
import Body from "./component/Body";
import Footer from "./component/Footer";

const Home = () => {
  return (
    <React.Fragment>
      <Header />
      <Body />
      <Footer />
    </React.Fragment>
  );
};

function App() {
  return <Home />;
}

export default App;
