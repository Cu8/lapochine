import "./body.css";
import React, { useState } from "react";
import Container from "../component/Container";
import GameModal from "./GameModal";
import RegisterModal from "./RegisterModal";
import axios from "../utils/axios";

const GameTutorial = ({ src, label }) => {
  return (
    <div className="game-banner">
      <img alt="g-dm" src={src} />
      <p>{label}</p>
    </div>
  );
};

const gameTutorial = [
  {
    src: require("../assets/gamedemo1.png"),
    label: "Chạm vào màn hình hoặc bấm chuột để điều khiển nhân vật lướt sóng.",
  },
  {
    src: require("../assets/gamedemo2.png"),
    label:
      "Né tránh các chướng ngại vật và thu thập các phần quà hấp dẫn từ Lapochine.",
  },
  {
    src: require("../assets/gamedemo3.png"),
    label:
      "Sau khi vượt qua thử thách lướt sóng người chơi chọn 1 trong 5 hộp quà may mắn để tìm kiếm voucher từ Lapochine Beach Resort",
  },
];

export default () => {
  const [showCanvas, setShowCanvas] = useState(false);
  const [showRegister, setShowRegister] = useState(false);

  return (
    <React.Fragment>
      <Container className="game-tutorial-container">
        <p>Thử thách lướt sóng cùng Lapochine</p>
        <div className="game-tutorial-content">
          {gameTutorial.map((e, i) => (
            <GameTutorial key={i} label={e.label} src={e.src} />
          ))}
        </div>

        <button
          onClick={() => {
            const token = localStorage.getItem("@userToken");
            if (!token) {
              setShowRegister(true);
            } else {
              setShowCanvas(true);
            }
          }}
        >
          Chơi ngay
        </button>

        <img alt="logo" src={require("../assets/logo-color.png")} />
        <p>Danh sách giải thưởng</p>
      </Container>
      {showCanvas && <GameModal />}
      {showRegister && (
        <RegisterModal
          onSubmit={async (formBody) => {
            const res = await axios.post("/register", formBody);
            const token = res.data;
            localStorage.setItem("@userToken", token);

            setShowRegister(false);
            setShowCanvas(true);
          }}
        />
      )}
    </React.Fragment>
  );
};
