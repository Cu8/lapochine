import React from "react";
import Container from "./Container";
import "./footer.css";

const content = [
  "Voucher giảm giá 99% đêm nghỉ phòng Deluxe",
  "Voucher giảm 50% dịch vụ Spa",
  "Voucher giảm 50% dịch vụ Ẩm thực",
  "Voucher 02 vé xem phim CGV",
];

export default () => {
  return (
    <footer>
      <Container className="footer-container" wrapperClassName="footer-wrapper">
        {content.map((e, i) => (
          <div className="row" key={i}>
            <img alt="star" src={require("../assets/star.png")} />
            <div className="row-label">
              <p>{e}</p>
              <div />
            </div>
          </div>
        ))}
      </Container>
    </footer>
  );
};
