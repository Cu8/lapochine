import "./GameModal.css";
import React, { useRef, useEffect } from "react";
import initGame from "../game";

export default () => {
  const canvasRef = useRef();
  useEffect(() => {
    if (canvasRef.current) {
      initGame(canvasRef.current);
    }
  }, [canvasRef]);

  return (
    <div className="game-modal">
      <div className="canvas-wrapper">
        <canvas ref={canvasRef}></canvas>
      </div>
    </div>
  );
};
