import React from "react";
import Container from "./Container";
import "./header.css";

export default () => {
  return (
    <header>
      <Container className="header-container">
        <div>
          <img alt="logo" src={require("../assets/logo.png")} />
          <p className="title">
            Đón sóng xanh <br /> Nhận quà thật đã
          </p>
          <p>
            Mừng Ana Mandara Huế Beach Resort & Spa ra mắt <br /> nhận diện
            thương hiệu mới Lapochine Beach Resort
          </p>
        </div>
      </Container>
    </header>
  );
};
