import "./RegisterModal.css";
import React, { useState } from "react";

// fullName, email, phoneNumber

export default ({ onSubmit }) => {
  const [state, setState] = useState({
    fullName: "",
    email: "",
    phoneNumber: "",
  });
  const [error, setError] = useState("");

  return (
    <div className="register-modal">
      <form
        onSubmit={async (e) => {
          e.preventDefault();
          try {
            await onSubmit(state);
          } catch (e) {
            setError("Email đã được sử dụng!");
          }
        }}
      >
        <img src={require("../assets/logo-color.png")} alt="logo" />
        <h1>Để lại thông tin cá nhân để tham gia thử thách</h1>
        <input
          placeholder="Họ tên"
          type="text"
          value={state.fullName}
          onChange={(e) => setState({ ...state, fullName: e.target.value })}
        />
        <input
          placeholder="Email"
          type="email"
          value={state.email}
          onChange={(e) => setState({ ...state, email: e.target.value })}
        />
        <input
          type="text"
          placeholder="Số điện thoại"
          value={state.phoneNumber}
          onChange={(e) => setState({ ...state, phoneNumber: e.target.value })}
        />
        {error && <p className="error">{error}</p>}
        <button type="submit">Bắt đầu</button>
      </form>
    </div>
  );
};
