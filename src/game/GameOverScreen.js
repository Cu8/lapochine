import { createBatch, createOrthoCamera, InputHandler } from 'gdxjs';
import { createRenderChoosingScreen } from './StartScreen';
import { checkPointInsideRect } from './utils/coordUtil';

export const createRenderGameOverScreen = Game => {
  const goToStartScreen = async () =>
    Game.setCurrentScreen(await createRenderChoosingScreen(Game));

  const { gl, width, height, canvas, assets, pixelRatio } = Game;

  const batch = createBatch(gl);
  const camera = createOrthoCamera(width, height, width, height);
  const inputHandler = new InputHandler(canvas);

  const atlas = assets.goAtlas;
  const bg = atlas.findRegion('surfing_game_asset_screen_4_GAMEOVER', -1);
  const button = atlas.findRegion('reload', -1);
  const title = atlas.findRegion('GAMEOVER', -1);

  const BG_HEIGHT = height;
  const BG_WIDTH = (BG_HEIGHT * bg.width) / bg.height;

  const BUTTON_WIDTH = width / 3;
  const BUTTON_HEIGHT = (BUTTON_WIDTH * button.height) / button.width;
  const BUTTON_RECT = [
    width / 2 - BUTTON_WIDTH / 2,
    (height * 2.9) / 4,
    BUTTON_WIDTH,
    BUTTON_HEIGHT
  ];

  const TITTLE_HEIGHT = height / 3;
  const TITTLE_WIDTH = (TITTLE_HEIGHT * title.width) / title.height;

  let point = {};
  inputHandler.addEventListener('touchStart', (x, y) => {
    point = { x: x, y: y };
    if (checkPointInsideRect(point, BUTTON_RECT, pixelRatio)) {
      goToStartScreen();
      Game.gender = 1;
    }
  });

  const update = delta => {
    camera.setPosition(0, 0);
    batch.setProjection(camera.combined);
    batch.begin();
    batch.setColor(1, 1, 1, 1);
    bg.draw(
      batch,
      width / 2 - BG_WIDTH / 2,
      height / 2 - BG_HEIGHT / 2,
      BG_WIDTH,
      BG_HEIGHT
    );

    button.draw(
      batch,
      width / 2 - BUTTON_WIDTH / 2,
      (height * 2.9) / 4,
      BUTTON_WIDTH,
      BUTTON_HEIGHT
    );

    title.draw(
      batch,
      width / 2 - TITTLE_WIDTH / 2,
      (height * 1.3) / 5,
      TITTLE_WIDTH,
      TITTLE_HEIGHT
    );

    batch.end();
  };

  return {
    update,
    destroy() {
      inputHandler.cleanup();
    }
  };
};
