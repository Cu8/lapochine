import { InputHandler } from 'gdxjs';
import createWorld from './createWorld';
import { createPlayerPositionSystem } from './system/PlayerPositionSystem';
import { createPlayerRenderSystem } from './system/PlayerRenderSystem';
// import { createWaterLineRenderSystem } from './system/WaterLineRenderSystem';
import { createObstacleSpawnSystem } from './system/ObstacleSpawnSystem';
import { createObstacleRenderSystem } from './system/ObstacleRenderSystem';
import { createObstacleCollideSystem } from './system/ObstacleCollideSystem';
import { createRenderRewardSystem } from './system/RewardRenderSystem';
import { createRewardSpawnSystem } from './system/RewardSpawnSystem';
import { createRewardCollideSystem } from './system/RewardCollideSystem';
import { createBgRenderSystem } from './system/BgRenderSystem';
import { createWaveRenderSystem } from './system/WaveRenderSystem';
import { createRenderGameOverScreen } from './GameOverScreen';
import { createRenderRewardScreen } from './RewardScreen';
import { createScoreRenderSystem } from './system/ScoreRenderSystem';

const createRenderPlayScreen = game => {
  const {
    gender,
    canvas,
    pixelRatio,
    width,
    height,
    gl,
    batch,
    camera,
    whiteTex,
    assets
  } = game;

  const inputHandler = new InputHandler(canvas);

  const GAP = width / 6;
  const PLAYER_INITIAL_Y = (2 * height) / 3;

  const worldState = {
    rewardCount: 0,
    goingUpTime: 0,
    win: false,
    gameOver: false,
    gender,
    stateTime: 0,
    playerState: {
      x: GAP,
      y: PLAYER_INITIAL_Y,
      speedX: 0,
      speedY: 0,
      width: width / 8,
      height: width / 7,
      jumping: false,
      stateTime: 0
    },
    waterY: height / 2.1 + height / 20,
    obstacles: [],
    rewards: [],
    gap: GAP,
    lastObstacleX: 2 * width,
    lastRewardX: 2 * width,
    obstacleInfos: {
      // [bird, rock1, rock2, seaStar]
      widths: [width / 9, width / 3, width / 3.5, width / 9],
      heights: [height / 20, height / 4, height / 4, width / 9],
      speedXs: [-width / 3, 0, 0, 0]
    },
    rewardInfos: {
      width: width / 9,
      height: width / 9
    },
    playerInfo: {
      fallingSpeed: height / 3,
      jumpingSpeed: -height * 1.2,
      jumpingDecelerator: height * 2,
      inWaterSpeedX: width / 2,
      onAirSpeedX: width
    }
  };
  const extra = {
    canvas,
    pixelRatio,
    width,
    height,
    gl,
    batch,
    camera,
    whiteTex,
    inputHandler,
    assets
  };
  const world = createWorld(worldState, extra);
  world.addSystem(createPlayerPositionSystem(), true);
  world.addSystem(createObstacleSpawnSystem(), true);
  world.addSystem(createObstacleCollideSystem(), true);
  world.addSystem(createRenderRewardSystem(), true);
  world.addSystem(createRewardCollideSystem(), true);

  world.addSystem(createBgRenderSystem());
  world.addSystem(createWaveRenderSystem());
  // world.addSystem(createWaterLineRenderSystem());
  world.addSystem(createObstacleRenderSystem());
  world.addSystem(createRewardSpawnSystem());
  world.addSystem(createPlayerRenderSystem());
  world.addSystem(createScoreRenderSystem());

  const goToGameOverScreen = async () =>
    game.setCurrentScreen(await createRenderGameOverScreen(game));

  const goToRewardScreen = async () => {
    game.setCurrentScreen(await createRenderRewardScreen(game));
  };

  let delayStateTime = 0;
  const update = delta => {
    if (worldState.rewardCount === 5) {
      delayStateTime += delta;
      world.update(delta);
      if (delayStateTime >= 0.5) {
        goToRewardScreen();
      }
    } else {
      if (!worldState.gameOver) {
        worldState.stateTime += delta;
        world.update(delta);
      } else {
        // world.update(delta, false);
        goToGameOverScreen();
      }
    }
  };

  return {
    update,
    destroy() {
      inputHandler.cleanup();
    }
  };
};

export default createRenderPlayScreen;
