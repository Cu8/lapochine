import { fillRect, InputHandler } from "gdxjs";
import createAnimation, { PlayMode } from "gdxjs/lib/createAnimation";
import { checkPointInsideRect } from "./utils/coordUtil";

import axios from "../utils/axios";

export const createRenderRewardScreen = (Game) => {
  const { width, height, camera, batch, assets, canvas, pixelRatio } = Game;

  const moquaAtlas = assets.moqua;
  const rewardAtlas = assets.reward;

  const inputHandler = new InputHandler(canvas);
  // back ground infor
  const bg = rewardAtlas.findRegion("endBG", -1);
  const logo = rewardAtlas.findRegion("logo", -1);
  const board = assets.rewardBoard;

  const BOARD_WIDTH = width;
  const BOARD_HEIGHT = (BOARD_WIDTH * board.height) / board.width;

  const LOGO_WIDTH = width / 2;
  const LOGO_HEIGHT = (LOGO_WIDTH * logo.height) / logo.width;

  const Y_BOARD = (height * 4.3) / 8 - BOARD_HEIGHT / 2;
  /// text infor
  const touchToOpen = rewardAtlas.findRegion("nhandemo", -1);
  const goodLuck = rewardAtlas.findRegion("goodluck", -1); // k mo ra
  const pingo = rewardAtlas.findRegion("nhanduoc", -1); // mo ra line 1
  const info = rewardAtlas.findRegion("thongtin", -1); // mo ra line 2

  const O_WIDTH = width / 1.7;
  const O_HEIGHT = (O_WIDTH * touchToOpen.height) / touchToOpen.width;
  const GL_WIDTH = width / 1.8;
  const GL_HEIGHT = (GL_WIDTH * goodLuck.height) / goodLuck.width;
  const PINGO_WIDTH = width / 2.1;
  const PINGO_HEIGHT = (PINGO_WIDTH * pingo.height) / pingo.width;
  const INFO_WIDTH = width / 2;
  const INFO_HEIGHT = (INFO_WIDTH * info.height) / info.width;

  // box infor
  const boxsShake = moquaAtlas.findRegions("moqua");
  const boxWin = moquaAtlas.findRegions("moqua_voucher");
  const boxLose = moquaAtlas.findRegion("moqua_empty", -1);
  const boxShakeAnimation = createAnimation(0.1, boxsShake);
  const boxWinDuration = 0.2;
  const otherWinBoxDuration = 0.15;
  const boxWinAnimation = createAnimation(boxWinDuration, boxWin);
  const otherWinBoxAnimation = createAnimation(otherWinBoxDuration, boxWin);
  const box = boxsShake[0];
  const BOX_WIDTH = width / 5.5;
  const BOX_HEIGHT = (BOX_WIDTH * box.height) / box.width;

  const BOX_POSITION = [
    [width / 2 - width / 100 - BOX_WIDTH, Y_BOARD + BOARD_HEIGHT * 0.49],
    [width / 2 + width / 100, Y_BOARD + BOARD_HEIGHT * 0.49],
    [width / 2 - width / 11 - BOX_WIDTH, Y_BOARD + BOARD_HEIGHT * 0.63],
    [width / 2 + width / 11, Y_BOARD + BOARD_HEIGHT * 0.63],
    [width / 2 - BOX_WIDTH / 2, Y_BOARD + BOARD_HEIGHT * 0.7],
  ];

  // hit box check
  const BOX_RECT = [
    [
      width / 2 -
        width / 100 -
        BOX_WIDTH +
        BOX_WIDTH / 2 -
        (BOX_WIDTH * 0.9) / 2,
      Y_BOARD + BOARD_HEIGHT * 0.49 + BOX_HEIGHT / 2 - (BOX_HEIGHT * 0.7) / 2,
      BOX_WIDTH * 0.9,
      BOX_HEIGHT * 0.8,
    ],
    [
      width / 2 + width / 100 + BOX_WIDTH / 2 - (BOX_WIDTH * 0.9) / 2,
      Y_BOARD + BOARD_HEIGHT * 0.49 + BOX_HEIGHT / 2 - (BOX_HEIGHT * 0.7) / 2,
      BOX_WIDTH * 0.9,
      BOX_HEIGHT * 0.8,
    ],
    [
      width / 2 -
        width / 11 -
        BOX_WIDTH +
        BOX_WIDTH / 2 -
        (BOX_WIDTH * 0.9) / 2,
      Y_BOARD + BOARD_HEIGHT * 0.63 + BOX_HEIGHT / 2 - (BOX_HEIGHT * 0.7) / 2,
      BOX_WIDTH * 0.9,
      BOX_HEIGHT * 0.8,
    ],
    [
      width / 2 + width / 11 + BOX_WIDTH / 2 - (BOX_WIDTH * 0.9) / 2,
      Y_BOARD + BOARD_HEIGHT * 0.63 + BOX_HEIGHT / 2 - (BOX_HEIGHT * 0.7) / 2,
      BOX_WIDTH * 0.9,
      BOX_HEIGHT * 0.8,
    ],
    [
      width / 2 - BOX_WIDTH / 2 + BOX_WIDTH / 2 - (BOX_WIDTH * 0.9) / 2,
      Y_BOARD + BOARD_HEIGHT * 0.7 + BOX_HEIGHT / 2 - (BOX_HEIGHT * 0.7) / 2,
      BOX_WIDTH * 0.9,
      BOX_HEIGHT * 0.8,
    ],
  ];

  const BOX_ARRAY = [
    { stateTime: 0, scale: 1 },
    { stateTime: 0, scale: 1 },
    { stateTime: 0, scale: 1 },
    { stateTime: 0, scale: 1 },
    { stateTime: 0, scale: 1 },
  ];

  let stateTimeWin = 0; // statTime for voucher animation
  let startRunOtherBox = false; // check when selected box finished animation
  let winningIndex = -1;
  let selectedIndex = -1;

  let point = {};
  inputHandler.addEventListener("touchStart", (x, y) => {
    point = { x: x, y: y };
    // winningIndex = -1;
    for (let i = 0; i < BOX_ARRAY.length; i++) {
      if (checkPointInsideRect(point, BOX_RECT[i], pixelRatio)) {
        selectedIndex = i;
        inputHandler.cleanup();
        break;
      }
    }
    if (selectedIndex > -1) {
      axios
        .post(
          "/roll",
          { rewardIndex: selectedIndex },
          {
            headers: {
              "x-access-token": localStorage.getItem("@userToken"),
            },
          }
        )
        .then((res) => (winningIndex = res.data.winningIndex));
      inputHandler.cleanup();
    }
  });

  inputHandler.addEventListener("touchMove", () => {});

  let state = 0; // text in context

  const update = (delta) => {
    camera.setPosition(0, 0);
    batch.setProjection(camera.combined);
    batch.begin();
    // draw background
    bg.draw(batch, 0, 0, width, height);
    logo.draw(
      batch,
      width / 2 - LOGO_WIDTH / 2,
      height / 15,
      LOGO_WIDTH,
      LOGO_HEIGHT
    );

    fillRect(
      batch,
      board,
      width / 2 - BOARD_WIDTH / 2,
      Y_BOARD,
      BOARD_WIDTH,
      BOARD_HEIGHT
    );
    // draw text
    if (state === 0) {
      touchToOpen.draw(
        batch,
        width / 2 - O_WIDTH / 2,
        Y_BOARD + BOARD_HEIGHT * 0.39,
        O_WIDTH,
        O_HEIGHT
      );
    } else if (state === 1) {
      goodLuck.draw(
        batch,
        width / 2 - GL_WIDTH / 2,
        Y_BOARD + BOARD_HEIGHT * 0.42,
        GL_WIDTH,
        GL_HEIGHT
      );
    } else if (state === 2) {
      pingo.draw(
        batch,
        width / 2 - PINGO_WIDTH / 2,
        Y_BOARD + BOARD_HEIGHT * 0.37,
        PINGO_WIDTH,
        PINGO_HEIGHT
      );
      info.draw(
        batch,
        width / 2 - INFO_WIDTH / 2,
        Y_BOARD + BOARD_HEIGHT * 0.88,
        INFO_WIDTH,
        INFO_HEIGHT
      );
    }

    // draw
    for (let i = 0; i < BOX_ARRAY.length; i++) {
      if (i === selectedIndex) {
        BOX_ARRAY[i].scale = 1.3;
      } else {
        if (selectedIndex !== -1) {
          BOX_ARRAY[i].scale = 0.8;
        }
      }

      if (i !== selectedIndex) {
        if (!startRunOtherBox) {
          // start unselected box
          box.draw(
            batch,
            BOX_POSITION[i][0],
            BOX_POSITION[i][1],
            BOX_WIDTH,
            BOX_HEIGHT,
            BOX_WIDTH / 2,
            BOX_HEIGHT / 2,
            0,
            BOX_ARRAY[i].scale,
            BOX_ARRAY[i].scale
          );
        } else {
          // unselected box after selected box animation
          if (i === winningIndex) {
            stateTimeWin += delta;
            if (stateTimeWin >= otherWinBoxDuration * (boxWin.length - 1)) {
              stateTimeWin = otherWinBoxDuration;
            }
            otherWinBoxAnimation
              .getKeyFrame(stateTimeWin, PlayMode.LOOP)
              .draw(
                batch,
                BOX_POSITION[i][0],
                BOX_POSITION[i][1],
                BOX_WIDTH,
                BOX_HEIGHT,
                BOX_WIDTH / 2,
                BOX_HEIGHT / 2,
                0,
                BOX_ARRAY[i].scale,
                BOX_ARRAY[i].scale
              );
          } else {
            boxLose.draw(
              batch,
              BOX_POSITION[i][0],
              BOX_POSITION[i][1],
              BOX_WIDTH,
              BOX_HEIGHT,
              BOX_WIDTH / 2,
              BOX_HEIGHT / 2,
              0,
              BOX_ARRAY[i].scale,
              BOX_ARRAY[i].scale
            );
          }
        }
      }

      if (i === selectedIndex) {
        // start animation after select box
        if (BOX_ARRAY[i].stateTime < 1.2 || winningIndex === -1) {
          BOX_ARRAY[i].stateTime += delta;
          boxShakeAnimation
            .getKeyFrame(BOX_ARRAY[i].stateTime, PlayMode.LOOP)
            .draw(
              batch,
              BOX_POSITION[i][0],
              BOX_POSITION[i][1],
              BOX_WIDTH,
              BOX_HEIGHT,
              BOX_WIDTH / 2,
              BOX_HEIGHT / 2,
              0,
              BOX_ARRAY[i].scale,
              BOX_ARRAY[i].scale
            );
        } else {
          // check winning box after animation
          if (i === winningIndex) {
            stateTimeWin += delta;
            if (stateTimeWin >= boxWinDuration * (boxWin.length - 1)) {
              state = 2;
              startRunOtherBox = true;
              stateTimeWin = boxWinDuration;
            }
            boxWinAnimation
              .getKeyFrame(stateTimeWin, PlayMode.LOOP)
              .draw(
                batch,
                BOX_POSITION[i][0],
                BOX_POSITION[i][1],
                BOX_WIDTH,
                BOX_HEIGHT,
                BOX_WIDTH / 2,
                BOX_HEIGHT / 2,
                0,
                BOX_ARRAY[i].scale,
                BOX_ARRAY[i].scale
              );
          } else {
            boxLose.draw(
              batch,
              BOX_POSITION[i][0],
              BOX_POSITION[i][1],
              BOX_WIDTH,
              BOX_HEIGHT,
              BOX_WIDTH / 2,
              BOX_HEIGHT / 2,
              0,
              BOX_ARRAY[i].scale,
              BOX_ARRAY[i].scale
            );
            state = 1;
            startRunOtherBox = true;
          }
        }
      }
      // batch.draw(
      //   whiteTex,
      //   BOX_RECT[i][0],
      //   BOX_RECT[i][1],
      //   BOX_RECT[i][2],
      //   BOX_RECT[i][3]
      // );
    }

    batch.end();
  };
  return {
    update,
    destroy() {
      inputHandler.cleanup();
    },
  };
};
