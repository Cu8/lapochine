import createScreen from "./utils/createScreen";
import { InputHandler } from "gdxjs";
import { checkPointInsideRect } from "./utils/coordUtil";
// import createWhiteTexture from 'gl-white-texture';
import createRenderPlayScreen from "./GameScreen";

export const createRenderChoosingScreen = (Game) => {
  const goToPlayScreen = async () =>
    Game.setCurrentScreen(await createRenderPlayScreen(Game));

  const { width, height, assets, canvas, pixelRatio, batch, camera } = Game;

  const inputHandler = new InputHandler(canvas);
  // const whiteTex = createWhiteTexture(gl);
  const atlas = assets.csAtlas;

  window.atlas = atlas;

  const bg = atlas.findRegion("bg", -1);

  const BG_HEIGHT = height;
  const BG_WIDTH = (BG_HEIGHT * bg.width) / bg.height;

  const button = atlas.findRegion("btn_play", -1);
  const BUTTON_WIDTH = width / 4;
  const BUTTON_HEIGHT = (BUTTON_WIDTH * button.height) / button.width;

  const choiceChar = atlas.findRegion("select_character", -1);
  const TITLE_WIDTH = (width * 2.81) / 4;
  const TITLE_HEIGHT = (TITLE_WIDTH * choiceChar.height) / choiceChar.width;

  const female_inactive = atlas.findRegion("fmale__inactive", -1);
  const FI_HEIGHT = height / 3.6;
  const FI_WIDTH = (FI_HEIGHT * female_inactive.width) / female_inactive.height;

  const female = atlas.findRegion("fmale", -1);
  const F_HEIGHT = height / 2.8;
  const F_WIDTH = (F_HEIGHT * female.width) / female.height;

  const male_inactive = atlas.findRegion("male_inactive", -1);
  const MI_HEIGHT = height / 3.6;
  const MI_WIDTH = (MI_HEIGHT * male_inactive.width) / male_inactive.height;

  const male = atlas.findRegion("male", -1);
  const M_HEIGHT = height / 2.8;
  const M_WIDTH = (M_HEIGHT * male.width) / male.height;

  const MALE_RECT = [
    width / 1.9 + width / 20,
    (height * 4.8) / 6 - MI_HEIGHT,
    MI_WIDTH,
    MI_HEIGHT,
  ];

  const FEMALE_RECT = [
    width / 1.9 - FI_WIDTH - width / 20,
    (height * 4.8) / 6 - MI_HEIGHT,
    FI_WIDTH,
    FI_HEIGHT,
  ];

  const BUTTON_RECT = [
    width / 2 - BUTTON_WIDTH / 2,
    (height * 6.2) / 7,
    BUTTON_WIDTH,
    BUTTON_HEIGHT,
  ];

  let femaleActive = true;
  let point;
  inputHandler.addEventListener("touchStart", (x, y) => {
    point = { x: x, y: y };
    if (checkPointInsideRect(point, MALE_RECT, pixelRatio)) {
      femaleActive = false;
      Game.gender = 0;
    }
    if (checkPointInsideRect(point, FEMALE_RECT, pixelRatio)) {
      femaleActive = true;
      Game.gender = 1;
    }
  });

  inputHandler.addEventListener("touchStart", (x, y) => {
    if (checkPointInsideRect(point, BUTTON_RECT, pixelRatio)) {
      goToPlayScreen();
    }
  });

  return createScreen({
    update(delta) {
      camera.setPosition(0, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      // background
      bg.draw(
        batch,
        width / 2 - BG_WIDTH / 2,
        height / 2 - BG_HEIGHT / 2,
        BG_WIDTH,
        BG_HEIGHT
      );
      // play button
      button.draw(
        batch,
        width / 2 - BUTTON_WIDTH / 2,
        (height * 6.2) / 7,
        BUTTON_WIDTH,
        BUTTON_HEIGHT
      );
      // choice char
      choiceChar.draw(
        batch,
        width / 2 - TITLE_WIDTH / 2,
        (height * 5.75) / 7,
        TITLE_WIDTH,
        TITLE_HEIGHT
      );
      // draw char
      if (femaleActive) {
        female.draw(
          batch,
          width / 1.9 - F_WIDTH,
          (height * 4.8) / 6 - F_HEIGHT,
          F_WIDTH,
          F_HEIGHT
        );
        male_inactive.draw(
          batch,
          width / 1.9 + width / 20,
          (height * 4.8) / 6 - MI_HEIGHT,
          MI_WIDTH,
          MI_HEIGHT
        );
      } else {
        male.draw(
          batch,
          width / 1.9,
          (height * 4.8) / 6 - M_HEIGHT,
          M_WIDTH,
          M_HEIGHT
        );
        female_inactive.draw(
          batch,
          width / 1.9 - FI_WIDTH - width / 20,
          (height * 4.8) / 6 - MI_HEIGHT,
          FI_WIDTH,
          FI_HEIGHT
        );
      }
      batch.end();
    },
    destroy() {
      inputHandler.cleanup();
    },
  });
};
