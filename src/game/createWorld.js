export default (worldState, extra) => {
  const systems = [];
  const passiveSystems = [];
  return {
    addSystem(system, passive = false) {
      system.init && system.init(worldState, extra);
      if (passive) {
        passiveSystems.push(system);
      } else {
        systems.push(system);
      }
    },
    update(delta, runPassive = true) {
      for (let system of systems) {
        system.update(delta, worldState, extra);
      }
      if (runPassive) {
        for (let system of passiveSystems) {
          system.update(delta, worldState, extra);
        }
      }
    }
  };
};
