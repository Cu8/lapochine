import {
  resizeCanvas,
  createGameLoop,
  createBatch,
  createOrthoCamera,
  loadTexture,
  loadAtlas,
} from "gdxjs";
import createWhiteTex from "gl-white-texture";
import { createRenderChoosingScreen } from "./StartScreen";

const init = async (canvas) => {
  const pixelRatio = window.devicePixelRatio || 1;
  const [width, height] = resizeCanvas(canvas, pixelRatio);
  const gl = canvas.getContext("webgl");
  // const info = document.getElementById('info');

  const batch = createBatch(gl);
  const camera = createOrthoCamera(width, height, width, height);
  const whiteTex = createWhiteTex(gl);

  const choosingScreenAtlas = await loadAtlas(gl, "./assets/surf.atlas");
  const playScreenAtlas = await loadAtlas(gl, "./assets/asset_wave.atlas");
  const waveAtlas = await loadAtlas(gl, "./assets/wave.atlas");
  const gameOver = await loadAtlas(gl, "./assets/end_surfing.atlas");
  const score = await loadAtlas(gl, "./assets/score.atlas");
  const moqua = await loadAtlas(gl, "./assets/moqua.atlas");
  const rewardAtlas = await loadAtlas(gl, "./assets/reward.atlas");
  const rewardBoard = await loadTexture(gl, "./rewardBoard.png");

  let assets = {
    csAtlas: choosingScreenAtlas,
    psAtlas: playScreenAtlas,
    waveAtlas: waveAtlas,
    goAtlas: gameOver,
    score: score,
    moqua: moqua,
    reward: rewardAtlas,
    rewardBoard: rewardBoard,
  };

  const Game = {
    gender: 1,
    width,
    height,
    canvas,
    gl,
    batch,
    camera,
    whiteTex,
    pixelRatio: pixelRatio,
    assets,
    setCurrentScreen(screen) {
      if (Game.currentScreen) {
        Game.currentScreen.destroy();
      }
      Game.currentScreen = screen;
    },
  };

  Game.setCurrentScreen(await createRenderChoosingScreen(Game));

  gl.clearColor(1, 0.388, 0.012, 1);
  // gl.clearColor(0, 0, 0, 1);
  // gl.clearColor(1, 0, 0, 1);
  const update = (delta) => {
    gl.clear(gl.COLOR_BUFFER_BIT);
    if (Game.currentScreen) {
      Game.currentScreen.update(delta);
    }
  };

  createGameLoop(update);
  // setInterval(() => (info.innerHTML = `FPS: ${loop.getFps()}`), 500);
};

export default init;
