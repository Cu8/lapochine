import createParallax, { createParallaxGroup } from '../utils/createParallax';
// import { fillRect } from 'gdxjs';

export const createBgRenderSystem = () => {
  let parallaxGroup;
  let backGround;
  return {
    init({ waterY }, { assets, width, height }) {
      const atlas = assets.psAtlas;
      const bgSky = atlas.findRegion('parallax_BG_sky', -1);
      const bgWater = atlas.findRegion('parallax_BG_water', -1);
      const cl1 = atlas.findRegion('parallax_BG_cloud1', -1);
      const cl2 = atlas.findRegion('parallax_BG_cloud2', -1);
      const cl3 = atlas.findRegion('parallax_BG_cloud3', -1);
      const cl4 = atlas.findRegion('parallax_BG_cloud4', -1);
      const islandTex = atlas.findRegion('parallax_BG_hondao', -1);
      const lightHouseTex = atlas.findRegion('parallax_BG_ngon_hai_dang', -1);
      backGround = bgSky;

      let waterYN = waterY + height / 200 - height / 20;
      // const bg = createParallax(bgSky, width, height, 0, 0, 0);
      const water = createParallax(
        bgWater,
        width,
        height,
        -width / 10,
        waterY - height / 20,
        0
      );
      const cloud1 = createParallax(
        cl1,
        width,
        height / 16,
        0,
        waterYN - height / 16,
        width / 10,
        width * 1.5
      );
      const cloud2 = createParallax(
        cl2,
        width,
        height / 11,
        0,
        waterYN - height / 11,
        width / 8,
        width * 1.5
      );
      const cloud3 = createParallax(
        cl3,
        width,
        height / 8,
        0,
        waterYN - height / 8,
        width / 6,
        width * 1.5
      );
      const cloud4 = createParallax(
        cl4,
        width,
        height / 6,
        0,
        waterYN - height / 6,
        width / 4,
        width * 1.5
      );
      const island = createParallax(
        islandTex,
        width,
        height / 30,
        0,
        waterYN - height / 30 + height / 200,
        width / 4,
        width
      );
      const lightHouse = createParallax(
        lightHouseTex,
        width,
        height / 20,
        width,
        waterYN - height / 20,
        width / 4,
        width * 5
      );
      parallaxGroup = createParallaxGroup();
      // parallaxGroup.add(bg);
      parallaxGroup.add(water);
      parallaxGroup.add(cloud1);
      parallaxGroup.add(cloud2);
      parallaxGroup.add(cloud3);
      parallaxGroup.add(cloud4);
      parallaxGroup.add(island);
      parallaxGroup.add(lightHouse);
    },
    update(delta, worldState, { batch, width, height, camera, assets }) {
      camera.setPosition(0, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      batch.setColor(1, 1, 1, 1);
      backGround.draw(batch, 0, 0, width, height);
      parallaxGroup.update(delta);
      parallaxGroup.draw(batch);
      batch.end();
    }
  };
};
