export const createObstacleCollideSystem = () => {
  return {
    update(delta, worldState) {
      const { playerState, obstacles, obstacleInfos } = worldState;
      if (obstacles.length > 0) {
        // console.log(obstacleInfos, obstacles);
        const { widths, heights } = obstacleInfos;
        const pL = playerState.x;
        const pR = playerState.x + playerState.width;
        const pT = playerState.y;
        const pB = playerState.y + playerState.height;
        for (let i = obstacles.length - 1; i >= 0; i--) {
          const { x, y, type } = obstacles[i];
          const oL = x;
          const oR = x + widths[type];
          const oT = y;
          const oB = y + heights[type];
          // console.log(pL, pR, pT, pB, oL, oR, oT, oB);
          if (pL < oR && pR > oL && pT < oB && pB > oT) {
            worldState.gameOver = true;
          }
        }
      }
    }
  };
};
