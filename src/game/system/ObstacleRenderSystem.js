import { createAnimation } from 'gdxjs';
import { PlayMode } from 'gdxjs/lib/createAnimation';
export const createObstacleRenderSystem = () => {
  let rock1Tex;
  let rock2Tex;
  let seaStarTex;
  let birdAnimation;
  let octopusAnimation;
  let crabAnimation;
  let BIRD_WIDTH,
    OCTOPUS_WIDTH,
    CRAB_WIDTH,
    STAR_WIDTH,
    ROCK1_WIDTH,
    ROCK2_WIDTH;
  let BIRD_HEIGHT,
    OCTOPUS_HEIGHT,
    CRAB_HEIGHT,
    STAR_HEIGHT,
    ROCK1_HEIGHT,
    ROCK2_HEIGHT;

  return {
    init({ obstacleInfos }, { assets, width, height }) {
      const atlas = assets.psAtlas;
      rock1Tex = atlas.findRegion('cuc_da', 1);
      rock2Tex = atlas.findRegion('cuc_da', 2);
      seaStarTex = atlas.findRegion('sao_bien', -1);
      const bird = atlas.findRegions('chim');
      const octopus = atlas.findRegions('bachtuoc');
      const crab = atlas.findRegions('cua');
      birdAnimation = createAnimation(0.15, bird);
      octopusAnimation = createAnimation(0.15, octopus);
      crabAnimation = createAnimation(0.15, crab);

      BIRD_WIDTH = width / 5;
      BIRD_HEIGHT = (bird[0].height * BIRD_WIDTH) / bird[0].width;
      ROCK1_HEIGHT = height / 3;
      ROCK1_WIDTH = (ROCK1_HEIGHT * rock1Tex.width) / rock1Tex.height;
      ROCK2_HEIGHT = height / 3;
      ROCK2_WIDTH = (ROCK2_HEIGHT * rock2Tex.width) / rock2Tex.height;
      OCTOPUS_HEIGHT = height / 6;
      OCTOPUS_WIDTH = (octopus[0].width * OCTOPUS_HEIGHT) / octopus[0].height;
      CRAB_HEIGHT = height / 11;
      CRAB_WIDTH = (crab[0].width * CRAB_HEIGHT) / crab[0].height;
      STAR_HEIGHT = height / 12;
      STAR_WIDTH = (STAR_HEIGHT * seaStarTex.width) / seaStarTex.height;
    },
    update(
      delta,
      {
        obstacles,
        playerState: { x },
        gap,
        obstacleInfos: { widths, heights }
      },
      { batch, camera, whiteTex, assets, width, height }
    ) {
      camera.setPosition(x - gap, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      for (let obstacle of obstacles) {
        const { type, x, y } = obstacle;
        // fillRect(batch, whiteTex, x, y, widths[type], heights[type]);
        if (type === 0) {
          obstacle.stateTime += delta;
          birdAnimation
            .getKeyFrame(obstacle.stateTime, PlayMode.LOOP_PINGPONG)
            .draw(
              batch,
              x + widths[type] / 2 - BIRD_WIDTH / 2,
              y + heights[type] / 2 - BIRD_HEIGHT / 2,
              BIRD_WIDTH,
              BIRD_HEIGHT
            );
        }
        if (type === 1) {
          obstacle.stateTime += delta;
          rock1Tex.draw(
            batch,
            x + widths[type] / 2 - ROCK1_WIDTH / 2 - width / 16,
            y + heights[type] - ROCK1_HEIGHT + height / 80,
            ROCK1_WIDTH,
            ROCK1_HEIGHT
          );
          crabAnimation
            .getKeyFrame(obstacle.stateTime, PlayMode.LOOP_PINGPONG)
            .draw(
              batch,
              x - CRAB_WIDTH / 2 + width / 20,
              y - CRAB_HEIGHT / 2 + height / 20,
              CRAB_WIDTH,
              CRAB_HEIGHT
            );
          seaStarTex.draw(
            batch,
            x + ROCK1_WIDTH / 5,
            y + ROCK1_HEIGHT / 2.7,
            STAR_WIDTH,
            STAR_HEIGHT
          );
        }
        if (type === 2) {
          obstacle.stateTime += delta;
          rock2Tex.draw(
            batch,
            x + widths[type] / 2 - ROCK2_WIDTH / 2 + width / 16,
            y + heights[type] - ROCK2_HEIGHT + height / 60,
            ROCK2_WIDTH,
            ROCK2_HEIGHT
          );
          octopusAnimation
            .getKeyFrame(obstacle.stateTime, PlayMode.LOOP_PINGPONG)
            .draw(
              batch,
              x - OCTOPUS_WIDTH / 2 + width / 20,
              y - OCTOPUS_WIDTH / 2,
              OCTOPUS_WIDTH,
              OCTOPUS_HEIGHT
            );
        }
        if (type === 3) {
          seaStarTex.draw(
            batch,
            x + widths[type] / 2 - STAR_WIDTH / 2,
            y + heights[type] / 2 - STAR_HEIGHT / 2,
            STAR_WIDTH,
            STAR_HEIGHT
          );
        }
      }
      batch.end();
    }
  };
};
