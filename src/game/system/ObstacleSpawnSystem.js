export const createObstacleSpawnSystem = () => {
  const generateObstacles = (
    obstacles,
    startX,
    endX,
    height,
    obstacleInfos,
    stopSpawnBird
  ) => {
    if (Math.random() > 0.4) {
      // bird
      obstacles.push({
        x: (endX + startX) / 2,
        y: Math.random() * (height / 3 - height / 8) + height / 8,
        type: 0,
        speedX: obstacleInfos.speedXs[0],
        stateTime: 0
      });
    }
    if (Math.random() > 0.5) {
      // rock1
      obstacles.push({
        x: (endX + startX) / 2,
        y: height - obstacleInfos.heights[1],
        type: 1,
        speedX: obstacleInfos.speedXs[1],
        stateTime: 0
      });
    } else {
      // rock2
      obstacles.push({
        x: (endX + startX) / 2,
        y: height - obstacleInfos.heights[2],
        type: 2,
        speedX: obstacleInfos.speedXs[2],
        stateTime: 0
      });
    }

    if (Math.random() > 0.3) {
      // sea star
      obstacles.push({
        x: endX,
        y: (height * 2.6) / 4,
        speedX: obstacleInfos.speedXs[3],
        type: 3
      });
    }
  };
  return {
    update(delta, worldState, { width, height }) {
      let {
        playerState: { x },
        obstacles,
        gap,
        lastObstacleX,
        obstacleInfos
      } = worldState;

      while (lastObstacleX - x < width * 2) {
        generateObstacles(
          obstacles,
          lastObstacleX + width * 2,
          lastObstacleX + width * 4,
          height,
          obstacleInfos,
          worldState.stopSpawnBird
        );
        lastObstacleX = worldState.lastObstacleX = lastObstacleX + width * 2;
      }

      for (let i = obstacles.length - 1; i >= 0; i--) {
        let { type, x } = obstacles[i];
        if (obstacles[i].x + worldState.obstacleInfos.widths[type] < x - gap) {
          obstacles.splice(i, 1);
        }
      }

      for (let obstacle of obstacles) {
        obstacle.x += obstacle.speedX * delta;
      }
    }
  };
};
