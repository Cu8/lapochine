export const createPlayerPositionSystem = inputhandler => {
  return {
    update(delta, worldState, extra) {
      const { inputHandler, height } = extra;
      const {
        playerState,
        waterY,
        playerInfo: {
          fallingSpeed,
          jumpingSpeed,
          jumpingDecelerator,
          inWaterSpeedX,
          onAirSpeedX
        }
      } = worldState;

      if (playerState.jumping) {
        playerState.speedY += jumpingDecelerator * delta;
        playerState.speedX = onAirSpeedX;
        if (playerState.y + playerState.height > waterY * 1.1) {
          playerState.jumping = false;
        }
      } else {
        playerState.speedX = inWaterSpeedX;
        if (inputHandler.isTouched()) {
          worldState.goingUpTime = worldState.stateTime + 0.15;
        }
        if (worldState.goingUpTime >= worldState.stateTime) {
          playerState.speedY = -fallingSpeed;
        } else {
          playerState.speedY = fallingSpeed / 2;
        }
        if (playerState.y + playerState.height < waterY * 0.9) {
          playerState.jumping = true;
          playerState.speedY = jumpingSpeed;
        }
      }

      playerState.x += playerState.speedX * delta;
      playerState.y += playerState.speedY * delta;

      if (playerState.y > height - playerState.height - height / 10) {
        playerState.y = height - playerState.height - height / 10;
      }
    }
  };
};
