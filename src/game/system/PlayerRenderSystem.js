// import { fillRect } from 'gdxjs';
import { createAnimation } from 'gdxjs';
import { PlayMode } from 'gdxjs/lib/createAnimation';
// import {}

export const createPlayerRenderSystem = () => {
  let PLAYER_WIDTH = 0;
  let PLAYER_HEIGHT = 0;
  let BUBBLE_WIDTH = 0;
  let BUBBLE_HEIGHT = 0;
  let PLAYER_JUMP_HEIGHT = 0;
  let asset;
  let atlas;
  let playerAnimation;
  let bubbleAnimation;
  let charJump;
  return {
    init({ gender, playerState }, { assets, width, height }) {
      atlas = assets.psAtlas;
      if (gender === 0) {
        asset = atlas.findRegions('male');
        charJump = atlas.findRegion('male_jump', -1);
      } else {
        asset = atlas.findRegions('female');
        charJump = atlas.findRegion('female_jump', -1);
      }
      const bubble = atlas.findRegions('bubble');

      bubbleAnimation = createAnimation(0.2, bubble);
      playerAnimation = createAnimation(0.3, asset);
      PLAYER_WIDTH = width / 3.2;
      PLAYER_HEIGHT = (asset[0].height * PLAYER_WIDTH) / asset[0].width;
      PLAYER_JUMP_HEIGHT = (charJump.height * PLAYER_WIDTH) / charJump.width;

      BUBBLE_WIDTH = width / 6;
      BUBBLE_HEIGHT = (bubble[0].height * BUBBLE_WIDTH) / bubble[0].width;
    },
    update(delta, { playerState, gap }, { batch, camera, whiteTex, width }) {
      camera.setPosition(playerState.x - gap, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      // fillRect(
      //   batch,
      //   whiteTex,
      //   playerState.x,
      //   playerState.y,
      //   playerState.width,
      //   playerState.height
      // );
      playerState.stateTime += delta;
      if (playerState.jumping) {
        charJump.draw(
          batch,
          playerState.x + playerState.width / 2 - PLAYER_WIDTH / 2,
          playerState.y + playerState.height / 2 - PLAYER_JUMP_HEIGHT / 2,
          PLAYER_WIDTH,
          PLAYER_JUMP_HEIGHT
        );
      } else {
        playerAnimation
          .getKeyFrame(playerState.stateTime, PlayMode.LOOP_PINGPONG)
          .draw(
            batch,
            playerState.x + playerState.width / 2 - PLAYER_WIDTH / 2,
            playerState.y + playerState.height / 2 - PLAYER_HEIGHT / 2,
            PLAYER_WIDTH,
            PLAYER_HEIGHT
          );
      }
      bubbleAnimation
        .getKeyFrame(playerState.stateTime, PlayMode.LOOP_PINGPONG)
        .draw(
          batch,
          playerState.x - PLAYER_WIDTH / 2,
          playerState.y + playerState.height,
          BUBBLE_WIDTH,
          BUBBLE_HEIGHT,
          BUBBLE_WIDTH / 2,
          BUBBLE_HEIGHT / 2,
          0,
          1,
          1
        );
      batch.end();
    }
  };
};
