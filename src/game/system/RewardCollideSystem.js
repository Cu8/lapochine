export const createRewardCollideSystem = () => {
  return {
    update(delta, worldState) {
      const { playerState, rewards, rewardInfos } = worldState;
      if (rewards.length > 0) {
        // console.log(obstacleInfos, obstacles);
        const pL = playerState.x;
        const pR = playerState.x + playerState.width;
        const pT = playerState.y;
        const pB = playerState.y + playerState.height;
        for (let i = rewards.length - 1; i >= 0; i--) {
          if (rewards[i].hit) {
            continue;
          }
          const rL = rewards[i].x;
          const rR = rewards[i].x + rewardInfos.width;
          const rT = rewards[i].y;
          const rB = rewards[i].y + rewardInfos.height;

          if (pL < rR && pR > rL && pT < rB && pB > rT) {
            rewards[i].hit = true;
            worldState.rewardCount += 1;
          }
        }
      }
    }
  };
};
