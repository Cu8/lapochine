// import { fillRect } from 'gdxjs';

export const createRenderRewardSystem = () => {
  let rewardText = 0;
  return {
    init(worldState, { assets, width, height }) {
      const atlas = assets.psAtlas;
      const reward2 = atlas.findRegion('hop_qua2', -1);
      rewardText = {
        texTure: reward2,
        width: width / 6,
        height: ((width / 6) * reward2.height) / reward2.width
      };
    },
    update(
      delta,
      { rewards, playerState: { x }, gap },
      { batch, width, height, camera, whiteTex }
    ) {
      camera.setPosition(x - gap, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      batch.setColor(1, 1, 1, 1);
      for (let reward of rewards) {
        if (!reward.hit) {
          // fillRect(
          //   batch,
          //   whiteTex,
          //   reward.x,
          //   reward.y,
          //   reward.width,
          //   reward.height
          // );
          rewardText.texTure.draw(
            batch,
            reward.x + reward.width / 2 - rewardText.width / 2,
            reward.y + reward.height / 2 - rewardText.height / 2,
            rewardText.width,
            rewardText.height
          );
        }
      }
      batch.end();
    }
  };
};
