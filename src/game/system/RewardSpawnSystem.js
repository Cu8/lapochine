export const createRewardSpawnSystem = () => {
  const generateRewards = (
    rewards,
    startX,
    endX,
    height,
    width,
    rewardInfos,
    worldState
  ) => {
    if (Math.random() > 0.6) {
      rewards.push({
        x: endX,
        y: height / 4,
        width: rewardInfos.width, // hit box size
        height: rewardInfos.width, // hit box size
        hit: false,
        type: Math.floor(Math.random() * 3),
        popUpScreen: false
      });
    } else {
      if (Math.random() > 0.9) {
        rewards.push({
          x: endX,
          y: (height * 6.6) / 8,
          width: rewardInfos.width, // hit box size
          height: rewardInfos.width, // hit box size
          hit: false,
          type: Math.floor(Math.random() * 3),
          popUpScreen: false
        });
      }
    }
  };
  return {
    update(delta, worldState, { width, height }) {
      let {
        playerState: { x },
        rewards,
        lastRewardX,
        rewardInfos
      } = worldState;

      while (lastRewardX - x < width * 2) {
        generateRewards(
          rewards,
          lastRewardX + width * 2,
          lastRewardX + width * 4,
          height,
          width,
          rewardInfos,
          worldState
        );
        lastRewardX = worldState.lastRewardX = lastRewardX + width * 2;
      }
      // for (let i = rewards.length - 1; i >= 0; i--) {
      //   if (rewards[i].x + worldState.obstacleInfos.width < x - gap) {
      //     rewards.splice(i, 1);
      //   }
      // }
    }
  };
};
