export const createScoreRenderSystem = () => {
  let scoreWidth = [];
  let scoreHeight = 0;
  let GIFT_WIDTH = 0;
  let GIFT_HEIGHT = 0;
  let X_WIDTH = 0;
  let X_HEIGHT = 0;
  let s0, s1, s2, s3, s4, s5, gift, x;

  return {
    init(worldState, { assets, width, height }) {
      const scoreAtlas = assets.score;
      s0 = scoreAtlas.findRegion('0_score', -1);
      s1 = scoreAtlas.findRegion('1_score', -1);
      s2 = scoreAtlas.findRegion('2_score', -1);
      s3 = scoreAtlas.findRegion('3_score', -1);
      s4 = scoreAtlas.findRegion('4_score', -1);
      s5 = scoreAtlas.findRegion('5_score', -1);
      scoreHeight = height / 26;
      scoreWidth = [
        (scoreHeight * s0.width) / s0.height,
        (scoreHeight * s1.width) / s1.height,
        (scoreHeight * s2.width) / s2.height,
        (scoreHeight * s3.width) / s3.height,
        (scoreHeight * s4.width) / s4.height,
        (scoreHeight * s5.width) / s5.height
      ];
      gift = scoreAtlas.findRegion('hopqua_score', -1);
      x = scoreAtlas.findRegion('x_score', -1);

      GIFT_HEIGHT = height / 16;
      GIFT_WIDTH = (GIFT_HEIGHT * gift.width) / gift.height;

      X_HEIGHT = height / 45;
      X_WIDTH = (X_HEIGHT * x.width) / x.height;
    },
    update(delta, worldState, { batch, camera, width, height }) {
      const GIFT_X = width / 16;
      const GIFT_Y = height / 50;
      camera.setPosition(0, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      gift.draw(batch, GIFT_X, GIFT_Y, GIFT_WIDTH, GIFT_HEIGHT);
      x.draw(
        batch,
        GIFT_X + (GIFT_WIDTH * 3.5) / 5,
        GIFT_Y + (GIFT_HEIGHT * 3.5) / 5,
        X_WIDTH,
        X_HEIGHT
      );

      let currentScore = 0;
      if (worldState.rewardCount === 0) currentScore = s0;
      if (worldState.rewardCount === 1) currentScore = s1;
      if (worldState.rewardCount === 2) currentScore = s2;
      if (worldState.rewardCount === 3) currentScore = s3;
      if (worldState.rewardCount === 4) currentScore = s4;
      if (worldState.rewardCount === 5) currentScore = s5;

      currentScore.draw(
        batch,
        GIFT_X + GIFT_WIDTH * 1.2,
        GIFT_Y + GIFT_HEIGHT - scoreHeight,
        scoreWidth[worldState.rewardCount],
        scoreHeight
      );

      batch.end();
    }
  };
};
