import { fillRect } from 'gdxjs';

export const createWaterLineRenderSystem = () => {
  return {
    update(delta, { waterY }, { width, height, batch, camera, whiteTex }) {
      camera.setPosition(0, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      fillRect(batch, whiteTex, 0, waterY, width, height / 100);
      batch.end();
    }
  };
};
