import { createAnimation } from 'gdxjs';
import { PlayMode } from 'gdxjs/lib/createAnimation';

export const createWaveRenderSystem = () => {
  let WAVE_WIDTH = 0;
  let WAVE_HEIGHT = 0;
  let stateTime = 0;
  let waveAnimation;
  return {
    init({ waterY }, { width, height, assets }) {
      const atlas = assets.waveAtlas;
      const waveAssets = atlas.findRegions('final wave');
      waveAnimation = createAnimation(0.1, waveAssets);
      WAVE_WIDTH = width;
      WAVE_HEIGHT = (waveAssets[0].height * WAVE_WIDTH) / waveAssets[0].width;
      if (waterY + WAVE_HEIGHT - height / 20 < height) {
        // console.log('low');
        WAVE_HEIGHT = height - (waterY - height / 20);
      }
    },
    update(delta, { waterY }, { batch, width, height, camera }) {
      stateTime += delta;
      camera.setPosition(0, 0);
      batch.setProjection(camera.combined);
      batch.begin();
      waveAnimation
        .getKeyFrame(stateTime, PlayMode.LOOP)
        .draw(batch, 0, waterY - height / 20, WAVE_WIDTH, WAVE_HEIGHT);
      batch.end();
    }
  };
};
