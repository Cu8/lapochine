export const checkPointInsideRect = (point, verts, DEVICE_PIXEL_RATIO = 1) => {
  const x = point.x * DEVICE_PIXEL_RATIO;
  const y = point.y * DEVICE_PIXEL_RATIO;
  // const minX = Math.min(verts[0], verts[2], verts[4], verts[6]);
  // const maxX = Math.max(verts[0], verts[2], verts[4], verts[6]);
  // const minY = Math.min(verts[1], verts[3], verts[5], verts[7]);
  // const maxY = Math.max(verts[1], verts[3], verts[5], verts[7]);

  const minX = verts[0];
  const maxX = verts[0] + verts[2];
  const minY = verts[1];
  const maxY = verts[1] + verts[3];
  if (x < minX || x > maxX || y < minY || y > maxY) return false;
  return true;
};
