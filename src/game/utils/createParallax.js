import loadTexture from 'gdxjs/lib/loadTexture';

const createParallax = (
  tex,
  width,
  height,
  startX,
  startY,
  speedX,
  gapX = 0
) => {
  const texWidth = tex.width;
  const texHeight = tex.height;
  const regionHeight = height;
  const regionWidth = (regionHeight / texHeight) * texWidth;
  let animateX = 0;

  return {
    update(delta) {
      animateX -= speedX * delta;
      while (animateX <= -(regionWidth + gapX)) {
        animateX += regionWidth + gapX;
      }
    },
    draw(batch) {
      let offsetX = animateX;
      while (offsetX < width) {
        // batch.draw(tex, startX + offsetX, startY, regionWidth, regionHeight);
        tex.draw(batch, startX + offsetX, startY, regionWidth, regionHeight);
        offsetX += regionWidth + gapX;
      }
    }
  };
};

export const loadParallax = async (
  gl,
  url,
  width,
  height,
  startX,
  startY,
  speedX
) => {
  const tex = await loadTexture(gl, url);
  return createParallax(tex, width, height, startX, startY, speedX);
};

export const createParallaxGroup = () => {
  const parallaxes = [];

  return {
    add: parallax => parallaxes.push(parallax),
    addAll: (...ps) => {
      for (let p of ps) {
        parallaxes.push(p);
      }
    },
    update(delta) {
      for (let parallax of parallaxes) {
        parallax.update(delta);
      }
    },
    draw(batch) {
      for (let parallax of parallaxes) {
        parallax.draw(batch);
      }
    }
  };
};

export default createParallax;
