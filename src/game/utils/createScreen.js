const noop = () => {};

const createScreen = ({ update = noop, destroy = noop }) => {
  return { update, destroy };
};

export default createScreen;
